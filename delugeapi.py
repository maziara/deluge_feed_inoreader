import config
from deluge_client import DelugeRPCClient
import werkzeug as wz
import base64

########################################################
###########        Deluge Methods       ################
########################################################

def connect_to_deluge():
    client = DelugeRPCClient(config.DLGD_HOST, config.DLGD_PORT, config.DLGD_USER, config.DLGD_PASS)
    client.connect()
    if client.connected: print("Connected to deluge daemon")

    from types import MethodType
    
    def create_torrent(self, path, tracker, comment, target, private, created_by, add_to_session):
        piece_length = 1024*1024 # 1MB
        webseeds = "" # Don't know, but works
        trackers = "" # Don't know, but works
        return self.call('core.create_torrent', path, tracker, piece_length, comment, target, webseeds, private, created_by, trackers, add_to_session)
    client.create_torrent = MethodType(create_torrent, client)

    def add_torr_url(self, url):
        return self.call('core.add_torrent_url', wz.urls.url_fix(url), {})
    client.add_torr_url = MethodType(add_torr_url, client)

    def add_torr_file(self, file):
        f = open(file, 'rb')
        filedump = base64.encodestring(f.read())
        f.close()
        return self.call('core.add_torrent_file', file, filedump, {})
    client.add_torr_file = MethodType(add_torr_file, client)
    
    def add_label(self, label, options={}):
        label = normalize_label(label)
        self.call('label.add', label)
        if options:
            if options['move_completed_path']:
                    options.update({'move_completed': True, 'apply_move_completed': True})
            self.call('label.set_options', label, options)
    client.add_label = MethodType(add_label, client)
    
    def label_exist(self, label):
        label = normalize_label(label)
        if label in self.list_labels():
            return True
        else:
            return False
    client.label_exist = MethodType(label_exist, client)            
        
    def list_labels(self):
        return self.call('label.get_labels')
    client.list_labels = MethodType(list_labels, client)
    
    def add_tor_label(self, tor_id, label):
        return self.call('label.set_torrent', tor_id, normalize_label(label))
    client.add_tor_label = MethodType(add_tor_label, client)

    def session_state(self):
        return self.call('core.get_session_state')
    client.session_state = MethodType(session_state, client)
    
    def torrent_status(self, tid, fields = {}):
        return self.call('core.get_torrent_status', tid, fields)
    client.torrent_status = MethodType(torrent_status, client)

    def torrents_status(self, filters = {}, fields = []):
        return self.call('core.get_torrents_status', filters, fields)
    client.torrents_status = MethodType(torrents_status, client)
    
    def get_unlabled(self):
        torrs = torrents_status(self)
        for k,v in torrs.items():
            #print(k,v['name'])
            if v[b'label'] != '':
                torrs.pop(k)
        return torrs
    client.get_unlabled = MethodType(get_unlabled, client)

    def get_finished(self):
        torrs = torrents_status(self)
        for v in list(torrs):
            #print(k,v['name'])
            if torrs[v][b'is_finished'] == False:
                #print("Removing unfinished: " + v['name'] + " " + str(v['is_finished']))
                torrs.pop(torrs[v][b'hash'])
            elif torrs[v][b'tracker_host'].decode() in config.REMOVE_SEEDS_EXCEPTION_TRACKERS:
                #print("Removing exception_tracker: " + v['name'])
                torrs.pop(torrs[v][b'hash'])
            elif not is_all_files_done(torrs[v]):
                #print("Removing not_all_done: " + v['name'])
                torrs.pop(torrs[v][b'hash'])
        return torrs
    client.get_finished = MethodType(get_finished, client)
    
    def remove_finished(self):
        for k in get_finished(self):
            self.call('core.remove_torrent', k, False)
    client.remove_finished = MethodType(remove_finished, client)

    def remove_torrent(self, torID, removeData=False):
        self.call('core.remove_torrent', torID, removeData)
    client.remove_torrent = MethodType(remove_torrent, client)
    
    def move_storage(self, torIDs, dest):
        self.call('core.move_storage', torIDs, dest)
    client.move_storage = MethodType(move_storage, client)
    
    def find_by_name(self, torName):
        # self.call('core.move_storage', torID, dest)
        allTorrents = self.torrents_status({},['name'])
        for torID in allTorrents:
            if (allTorrents[torID][b'name'].decode() == torName):
                return torID 
        return False
    client.find_by_name = MethodType(find_by_name, client)
    
    def is_all_files_done(tor):
        for i in tor[b'file_progress']:
            if i != 1.0: 
                return False
        return True

    return client

def normalize_label(label):
    return label.replace(' ','_').lower()
