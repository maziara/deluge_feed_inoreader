#!/homeZFS/maziar/.local/share/virtualenvs/deluge_feed_inoReader-mOixcAHm/bin/python
import codecs, shlex
from datetime import datetime
import deluge_feed_inoReader.delugeapi

KeepingDestination = "/homeZFS/maziar/Downloads/P/~Keep"
PictureKeepingDestination = "/homeZFS/maziar/Downloads/P/~Photos/~New"

def parseDescriptionFile():
    unProcessedItems = []
    DescriptionFilePath = "/homeZFS/maziar/Downloads/P/EMP/descript.ion"
    with codecs.open(DescriptionFilePath, "r", encoding='cp1252') as DescriptionFile:
        for line in DescriptionFile:
            a = shlex.split(line)
            moveDest = None
            if a[1] == "R":
                print("{} removing '{}'".format(a[1], a[0]))
            elif a[1] == "K":
                print("{} keeping '{}'".format(a[1], a[0]))
                moveDest = KeepingDestination
            elif a[1] == "P":
                print("{} picture keeping '{}'".format(a[1], a[0]))
                moveDest = PictureKeepingDestination
            else:
                unProcessedItems.append(line)
                continue
            torID = client.find_by_name(a[0])
            if torID == False:
                unProcessedItems.append(' '.join(a) + '_NotFound\r\n')
                continue
            if moveDest != None:
                client.move_storage([torID], moveDest) # This method accepts an array of IDs
                client.remove_torrent(torID, False)
            else:
                client.remove_torrent(torID, True)

    #writeUnprocessedFles():
    with codecs.open(DescriptionFilePath, "w", encoding='cp1252') as DescriptionFile:
        for item in unProcessedItems:
            DescriptionFile.write(item)

# --- MAIN ---
try:
    client = deluge_feed_inoReader.delugeapi.connect_to_deluge()
except Exception as e:
    print("Error: Could not connect to deluge!")
    print(e)
else:
    parseDescriptionFile()
